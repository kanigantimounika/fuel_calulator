
const FUEL_TYPE_ARRAY = { "Petrol": "0", "Diesel": "1" }
const DISTRICT_API = "http://localhost:3000/districts"
const DISTRICT_ID = "district"
const FUEL_ID = "fuel_type"
const PRICE_ID="priceid"
const FUEL_TYPE_DROPDOWN_DEFAULT = "Fuel Type"
const MILEAGE="mileage"
const KM="km"
const RESULT="result"

var districtindex;

var value1;




const onLoad = async () => {

  let response = await districtdropdown();
  let data = await response.json();

  getDistrictDropDown(data);

}

const getDistrictDropDown = (data) => {

  let districtDropdown = document.getElementById(DISTRICT_ID);
  let fragmentDistrictDropdown = document.createDocumentFragment();

  data.forEach((currentDistrict) => {

    let option = document.createElement('option');
    option.text = currentDistrict.district;
    option.value = currentDistrict.district;
    option.id = currentDistrict.district;
    fragmentDistrictDropdown.appendChild(option)

  })
  districtDropdown.appendChild(fragmentDistrictDropdown);

  districtDropdown.addEventListener("change", function (event) {
    getFuelDropDown(event.target.value, data);
  })
 

}


const getPrice=(value,fueldata)=>{

  let price=fueldata.find(curn=>curn.productName==value).productPrice;
  let priceText=document.getElementById(PRICE_ID); 
  priceText.value=price;
 
}

const getFuelDropDown = (selectedDistrict, data) => {

  clearFuelTypeDrowDown();
  let fuelData = data.find(crnt => crnt.district == selectedDistrict).products;
  let fuelDropDown = document.getElementById(FUEL_ID);
  let fragmentFuelType = document.createDocumentFragment();
  fuelData.forEach((currentvalue) => {
  let optionSelected = document.createElement('option');
  optionSelected.id = currentvalue.productName;
  optionSelected.value = currentvalue.productName;
  optionSelected.text = currentvalue.productName;
  fragmentFuelType.appendChild(optionSelected);
  })
  fuelDropDown.appendChild(fragmentFuelType);   
  fuelDropDown.addEventListener("change",function (event) {
    getPrice(event.target.value,fuelData)
  })

}


const districtdropdown = () => {
  try {
    return fetch(DISTRICT_API);

  }
  catch (exception) {
    console.log("Error in fetching API data" + exception)
  }

}

const clearFuelTypeDrowDown = () => {
  let dropdownExisting = document.getElementById(FUEL_ID);
  let newDropdown = document.createElement('select');
  newDropdown.id = FUEL_ID;
  let defaultOption = document.createElement('option');
  defaultOption.text = FUEL_TYPE_DROPDOWN_DEFAULT
  newDropdown.appendChild(defaultOption);
  dropdownExisting?.replaceWith(newDropdown);

}



function calculate()
{

   let km=document.getElementById(KM).value;

    let mileage=document.getElementById(MILEAGE).value;
    let petrolCost=document.getElementById(PRICE_ID).value;
    let totalConsumption=km*(mileage/100);
    let cost=totalConsumption*petrolCost
    var h1=document.createElement("h2");
    var textbox1=document.createTextNode("Total cost"+cost);
    h1.setAttribute("id","costcalculated");
    h1.appendChild(textbox1);
    document.getElementById(RESULT).appendChild(h1);
}
